from time import sleep
from celery import shared_task
from celery.signals import task_success
from scraper.serializers import postSerializer
import requests


@shared_task
def scraping_task(title, location):
    print("scraping in process.")
    sleep(2)
    data = [
        {
            "title": "Assistant Chef",
            "company": "XYZ",
            "location": "Seattle"
        },
    ]
    print("scraping done.")
    return data


@task_success.connect(sender=scraping_task, weak=False)
def save_to_db_task(sender=None, **kwargs):
    print("reach store to db!!!!!!!!!")
    posts = kwargs.get("result")
    # requests.post("http://127.0.0.1:8000/api/scraper/save",
    #               json={"posts": data})
    for post in posts:
        s = postSerializer(data=post)
        if s.is_valid():
            s.save()
    print("save data success!")
