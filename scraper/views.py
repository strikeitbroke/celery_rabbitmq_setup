from scraper.tasks import scraping_task, save_to_db_task
from scraper.models import Post
from scraper.serializers import postSerializer
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response


class ScraperList(APIView):

    def get(self, request):
        posts = Post.objects.all()
        s = postSerializer(
            posts,
            many=True,
            context={"request": request})
        return Response(s.data,
                        status=status.HTTP_200_OK)

    def post(self, request, format=None):

        res = scraping_task.delay("chef", "Seattle")
        return Response({"result": str(res)},
                        status=status.HTTP_201_CREATED)


class StoreDBList(APIView):

    def post(self, request, format=None):
        posts = request.data.get("posts")
        for post in posts:
            s = postSerializer(data=post)
            if s.is_valid():
                s.save()
            else:
                return Response({"result": "failed"},
                                status=status.HTTP_400_BAD_REQUEST)
        return Response({"result": "success"},
                        status=status.HTTP_201_CREATED)
