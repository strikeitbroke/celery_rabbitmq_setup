from scraper.models import Post
from rest_framework import serializers


class postSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = [
            "id", "title", "company", "location"
        ]
